<%@ page errorPage="myerror.jsp" contentType="text/html; charset=utf-8" %>
<!DOCTYPE html>
<html lang="en">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/json2/20150503/json2.js"></script>
<head>
<title>Hello JIRA</title>
<configuration>
 <system.webServer>
   <httpProtocol>
     <customHeaders>
       <add name="Access-Control-Allow-Origin" value="*" />
       <add name="Access-Control-Allow-Credentials" value="true" />
     </customHeaders>
   </httpProtocol>
 </system.webServer>
</configuration>
<style>
p {
	color: blue;
	margin: 5px;
	cursor: pointer;
}

p:hover {
	background: yellow;
}
</style>
<script type="text/javascript">
 	function load() {
		var xmlhttp;
		xmlhttp = new XMLHttpRequest();
		//getXMLHttpRequest();
		xmlhttp.open("POST", "http://192.168.32.140:8080/login.jsp?os_username=admin&os_password=admin",false);
		xmlhttp.send(null);
		sleep(2000);
		
	}  

	$(document).ready(function() {	
			var xmlhttp;
            var xmlhttp_worklog;
			var xmlhttp_web;
            $("#loadproject").click(function() {
				$("#project_list").empty();
                xmlhttp=new XMLHttpRequest();
				xmlhttp.open("GET", "http://192.168.32.140:8080/rest/api/2/project",false);
                xmlhttp.setRequestHeader("Content-type", "application/json");
		        xmlhttp.send(null);
		        var myProject = JSON.parse(xmlhttp.responseText); //json
		        for (var i = 0; i < myProject.length; i++) {
		            jQuery("#project_list").append("<option value='" + myProject[i].key +"'>"+ myProject[i].key + "</option>");
		        }
			});
			
			$("#project_list").change(function(){
				$("#jira_worklog").empty();
				xmlhttp_worklog = new XMLHttpRequest();
				xmlhttp_worklog.open("GET","http://192.168.32.140:8080/rest/jira-worklog-query/1/find/worklogs?startDate="+ $("#startdate").val() +"&endDate="+ $("#lastdate").val() +"&group=jira-software-users&project=" + $("#project_list").val(),false);
		        xmlhttp_worklog.setRequestHeader("Content-type","application/json");
		        xmlhttp_worklog.send(null);
				var worklog = "";
		        var myWork = JSON.parse(xmlhttp_worklog.responseText);					
				$("#jira_worklog").val(JSON.stringify(myWork));
				
            });

			$("#createweb").click(function(){
				var myWork = "";
				xmlhttp_export = new XMLHttpRequest();
				xmlhttp_export.open("GET","http://192.168.32.140:8080/rest/jira-worklog-query/1/find/worklogs?startDate="+ $("#startdate").val() +"&endDate="+ $("#lastdate").val() +"&group=jira-software-users&project=" + $("#project_list").val(),false);
		        xmlhttp_export.setRequestHeader("Content-type","application/json");
		        xmlhttp_export.send(null);
		        var myWork = JSON.parse(xmlhttp_export.responseText);				

				xmlhttp_web = new XMLHttpRequest();
				xmlhttp_web.addEventListener("load", reqListener);
				xmlhttp_web.open("POST", $("#WebhookURL").val(),true);
				xmlhttp_web.setRequestHeader('Content-Type', 'application/json');
				xmlhttp_web.send(JSON.stringify(myWork));

            });			
		});
		
	 function reqListener () {
        console.log(this.responseText);
		alert(" WebHook has been created!");
    }
		
	 function sleep(ms) {
		var dt = new Date();
		dt.setTime(dt.getTime() + ms);
		while (new Date().getTime() < dt.getTime())
			;
	}; 
		
</script>

</head>
<body onload=load()>
	<div class="container">
		<h1 class="text-center">Hello! JIRA World!</h1>
		<span class="label label-default">RestAPI Demo:2018_0701</span>
		<h3>
		  <label for="bookdate">開始日期：</label> <input type="date" id="startdate" required 
pattern="(?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:(?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))" placeholder="2010-01-01"> 
		  <label for="bookdate">結束日期：</label> <input type="date" id="lastdate" required 
pattern="(?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:(?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))" placeholder="2030-01-01"> 
		</h3>
		<div>
		  <button id="loadproject" type="button" class="btn btn-default"> Load Projects </button>
		</div>
		<h3 class="col-md-5">
				<select id="project_list" class="form-control"><!-- <select multiple id="project_list" class="form-control"> -->
					<option value=""> All_Project </option>
				</select>
		</h3>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">Project all Worklog</div>
					<div class="panel-body">
						<form role="form">
							<div class="form-group col-md-12">
								<textarea id="jira_worklog" class="form-control" cols="40" rows="5" placeholder="顯示 Worklog"></textarea>
							</div>
						</form>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">Webhook URL</div>	
					<div class="panel-body">
						<form role="form">
							<div>
							<textarea id="WebhookURL" class="form-control" placeholder="Webhook_URL"></textarea>
							</div>
							<div>
							<button id="createweb" type="button" class="btn btn-default">Create Webhook</button>
							</div>
						</form>						
					</div>
				</div>
			</div>
		</div>
		<div class="row">
            <div class="col-md-12">
                <span class="label label-primary">WebHook Demo:</span>
                <span class="label label-info">
                <a href="https://beeceptor.com/" target="_blank">https://beeceptor.com/</a>
                </span>
            </div>
        </div>
		<div class="download">
		</div>
        <div class="footer">
            <div class="col-md-12 text-center">
                Copyright © 2018 Vic, Linktech Inc. All rights reserved
            </div>
        </div>
	</div>
</body>
</html>
