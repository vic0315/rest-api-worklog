<%@ page errorPage="myerror.jsp" contentType="text/html; charset=utf-8" %>
<!DOCTYPE html>
<html lang="en">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/json2/20150503/json2.js"></script>
<head>
<title>Hello JIRA</title>
<style>
p {
	color: blue;
	margin: 5px;
	cursor: pointer;
}

p:hover {
	background: yellow;
}
</style>
<script type="text/javascript">
 	function load() {
		var xmlhttp;
		xmlhttp = new XMLHttpRequest();
		//getXMLHttpRequest();
		xmlhttp.open("POST", "http://192.168.164.129/login.jsp?os_username=leon&os_password=leon",false);
		xmlhttp.send(null);
		sleep(2000);
		
	}  

	$(document).ready(function() {	
			var xmlhttp;
            var xmlhttp_worklog;			
            $("#loadproject").click(function() {
                xmlhttp=new XMLHttpRequest();
				xmlhttp.open("GET", "http://192.168.164.129/rest/api/2/project",false);
                xmlhttp.setRequestHeader("Content-type", "application/json");
		        xmlhttp.send(null);
		        var myProject = JSON.parse(xmlhttp.responseText); //json
		        for (var i = 0; i < myProject.length; i++) {
		            jQuery("#project_list").append("<option value='" + myProject[i].key +"'>"+ myProject[i].name + "</option>");
		        }
			});
			
			$("#project_list").change(function(){
		var count=0;
                $("#the_issues").empty();
                $("#the_issues").append("<option value=''>please select issue</option>");
                if ($("#project_list").val()==""){
                    /* DO NOTHING */
	            }
		        else{
		            for (var j=1;j<10000;j++){
		                xmlhttp.open("GET","http://192.168.164.129/rest/api/2/issue/" + $("#project_list").val() + "-" + j,false);
		                xmlhttp.setRequestHeader("Content-type","application/json");
		                xmlhttp.send(null);
		                var myIssue = JSON.parse(xmlhttp.responseText);
		                if (typeof myIssue.key == "string"){
				    count=0;
		                    $("#the_issues").append("<option value='" + myIssue.key +"'>" + myIssue.key + "</option>");
		                }
				else {
					count++;
					if (count==10) {
					    break;
					}
				}
		            }
		        }
		        
            });
            
            $("#the_issues").change(function(){
		        if ($("#the_issues").val()==""){
		            /* DO NOTHING */
		        }
		        else{
		            xmlhttp.open("GET","http://192.168.164.129/rest/api/2/issue/" + $("#the_issues").val(),false);
		            xmlhttp.setRequestHeader("Content-type","application/json");
		            xmlhttp.send(null);
					var component = "";
		            var myObject = JSON.parse(xmlhttp.responseText);
				            
		            try{
		                $("#jira_id_project").val($("#project_list").val());
		            }
		            catch(e){
		                $("#jira_id_project").val("");
		            }
		            try{
		                $("#jira_assignee").val(JSON.stringify(myObject.fields.assignee.name));
		            }
		            catch(e){
		                $("#jira_assignee").val("--");
		            }
					try{
		                for (var k=0;k < myObject.fields.components.length;k++)				
					{							
						if (k==0){
							component = component + JSON.stringify(myObject.fields.components[k].name);
						}
						else {
							component = component + "," + JSON.stringify(myObject.fields.components[k].name);
						}
					}
					$("#jira_component").val(component);
		            }
		            catch(e){
		                $("#jira_component").val("--");
		            }
		            try{
		                $("#jira_summary").val(myObject.fields.summary);
		            }
		            catch(e){
		                $("#jira_summary").val("");
		            }
		            try{
		                $("#jira_description").val(myObject.fields.description);
		            }
		            catch(e){
		                $("#jira_description").val("");
		            }
		            try{
		                $("#jira_labels").val(myObject.fields.labels[0]);
		            }
		            catch(e){
		                $("#jira_labels").val("");
		            }
		            try{
		                $("#jira_model_no").val(myObject.fields.customfield_10301);
		            }
		            catch(e){
		                $("#jira_model_no").val("");
		            }
					//myWork.worklogs[k].author.name + "," + myWork.worklogs[k].created + "," + myWork.worklogs[k].timeSpent//
					xmlhttp_worklog = new XMLHttpRequest();
					xmlhttp_worklog.open("GET","http://192.168.164.129/rest/api/2/issue/" + $("#the_issues").val() + "/worklog",false);
		            xmlhttp_worklog.setRequestHeader("Content-type","application/json");
		            xmlhttp_worklog.send(null);
					var worklog = "";
		            var myWork = JSON.parse(xmlhttp_worklog.responseText);
					
					try{
		                for (var k=0;k < myWork.worklogs.length;k++)					
					{							
						if (k==0){
							worklog = JSON.stringify(myWork.worklogs[k].author.name + "," + myWork.worklogs[k].created + "," + myWork.worklogs[k].timeSpent);
						}
						else {
							worklog = worklog + "\n" + JSON.stringify(myWork.worklogs[k].author.name + "," + myWork.worklogs[k].created + "," + myWork.worklogs[k].timeSpent);
						}
					}
					$("#jira_worklog").val(worklog);
		            }
		            catch(e){
		                $("#jira_worklog").val("no_data");
		            }
		        }
		    });
 
		    $("#but_submit").click(function(){
		        xmlhttp.open("PUT","http://192.168.164.129/rest/api/2/issue/" + $("#the_issues").val() + "",false);
		        xmlhttp.setRequestHeader("Content-type","application/json");
			var trans_component;
			var component_data="";
			trans_component=$("#jira_component").val().split(",");
			for (var t=0;t < trans_component.length;t++) {
			    if (t>0){
				component_data = component_data + ",";
			    }
			    component_data = component_data + "{\"name\":"+trans_component[t]+"}";
			}
		        var data = "{\"update\": {\"components\":[{\"set\":["+ component_data +"]}]},\"fields\": {   \
		                \"summary\":\"" + $("#jira_summary").val() + "\",   \
		                \"assignee\":{\"name\": \"" + $("#sel_name").val() + "\"},   \
		                \"description\":\"" + $("#jira_description").val() + "\" \
		            }   \
		        }";
		            
		        xmlhttp.send(data);
				xmlhttp_worklog.send(data);
		        alert(xmlhttp.responseText);
		        alert($("#the_issues").val()+"modify finish");
		    });
		    
		    $("#submit_create").click(function(){
		        xmlhttp=new XMLHttpRequest();
                xmlhttp.open("POST","http://192.168.164.129/rest/api/2/issue/",false);
                xmlhttp.setRequestHeader("Content-type","application/json");
                var createdata = "{   \
		            \"fields\": {   \
		                \"project\":{\"key\": \"" + $("#project_list").val() + "\"}, \
		                \"summary\": \"" + $("#new_issue").val() + "\",  \
		                \"description\":\"" + $("#new_description").val() + "\",   \
		                \"issuetype\":{\"name\": \"" + $("#sel_issuetype").val() + "\"} \
		            }   \
		        }";
                xmlhttp.send(createdata);
                var newissue = xmlhttp.responseText;
                alert(newissue);
                alert(JSON.stringfy(newissue.name)+"has created!");
            });
	});
	 function sleep(ms) {
		var dt = new Date();
		dt.setTime(dt.getTime() + ms);
		while (new Date().getTime() < dt.getTime())
			;
	}; 
</script>

</head>
<body onload=load()>
	<div class="container">
		<h1 class="text-center">Hello! JIRA World!</h1>
		<span class="label label-default">RestAPI Demo:2018_0619</span>
		<h2>
		  <button id="loadproject" type="button" class="btn btn-default"> Load Projects </button>
		</h2>
		<div class="row">
			<h4 class="col-md-5">
					<select id="project_list" class="form-control"><!-- <select multiple id="project_list" class="form-control"> -->
						<option value=""> please select project </option>
					</select>
			</h4>
			<h4 class="col-md-4">
					<select id="the_issues" class="form-control">
						<option value=""> issues </option>
					</select>
			</h4>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">Issue 內容</div>
					<div class="panel-body">
						<form role="form">
							<div class="form-group col-md-12">
								<label for="jira_summary">Summary</label> <input type="text"
									class="form-control" id="jira_summary" placeholder="顯示 Summary">
							</div>
							<div class="form-group col-md-2">
								<label for="jira_id_project">Project Id</label> <input
									type="text" class="form-control" id="jira_id_project"
									placeholder="顯示 Project Id">
							</div>
                            <div class="form-group col-md-2">
                                <label for="jira_assignee">Assignee</label> <input
                                    type="text" class="form-control" id="jira_assignee"
                                    placeholder="顯示 Assignee">
                            </div>
							<div class="form-group col-md-2">
							    <label for="sel_name">Assignee Change</label>
								<select id="sel_name" class="form-control">
									<option value="">choose users</option>
									<option value="bho">bho</option>
									<option value="sapphire">sapphire</option>
									<option value="leon">leon</option>
									<option value="pm">pm</option>
								</select>
							</div>
							<div class="form-group col-md-6">
								<label for="jira_component">Component</label> <input
									type="text" class="form-control" id="jira_component"
									placeholder="顯示 Componnet">
							</div>
							<div class="form-group col-md-12">
								<label for="jira_description">Description</label> <input
									type="text" class="form-control" id="jira_description"
									placeholder="顯示 Description">
							</div>
							<div class="form-group col-md-12">
								<label for="jira_worklog">Worklog</label> <textarea id="jira_worklog" class="form-control" cols="40" rows="5" placeholder="顯示 Worklog"></textarea>
							</div>
							<div class="form-group col-md-12">
							 <button type="submit" id="but_submit" class="btn btn-default"> Update </button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
            <div class="col-md-12">
                <span class="label label-primary">WebHook Demo:</span>
                <span class="label label-info">
                <a href="http://requestb.in/1ffhf131?inspect" target="_blank">http://requestb.in/1ffhf131?inspect</a>
                </span>
            </div>
        </div>
        <div class="footer">
            <div class="col-md-12 text-center">
                Copyright © 2018 Vic, Linktech Inc. All rights reserved
            </div>
        </div>
	</div>
</body>
</html>
