<%@ page errorPage="myerror.jsp" contentType="text/html; charset=utf-8" %>
<!DOCTYPE html>
<html lang="en">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap-theme.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/json2/20150503/json2.js"></script>
<head>
<title>Hello JIRA</title>
<style>
p {
	color: blue;
	margin: 5px;
	cursor: pointer;
}

p:hover {
	background: yellow;
}
</style>
<script type="text/javascript">
 	function load() {
		var xmlhttp;
		xmlhttp = new XMLHttpRequest();
		//getXMLHttpRequest();
		xmlhttp.open("POST", "http://192.168.164.129/login.jsp?os_username=leon&os_password=leon",false);
		xmlhttp.send(null);
		sleep(2000);
		
	}  

	$(document).ready(function() {	
			var xmlhttp;
            var xmlhttp_worklog;			
            $("#loadproject").click(function() {
                xmlhttp=new XMLHttpRequest();
				xmlhttp.open("GET", "http://192.168.164.129/rest/api/2/project",false);
                xmlhttp.setRequestHeader("Content-type", "application/json");
		        xmlhttp.send(null);
		        var myProject = JSON.parse(xmlhttp.responseText); //json
		        for (var i = 0; i < myProject.length; i++) {
		            jQuery("#project_list").append("<option value='" + myProject[i].key +"'>"+ myProject[i].key + "</option>");
		        }
			});
			
			$("#project_list").change(function(){
				$("#jira_worklog").empty();
				xmlhttp_worklog = new XMLHttpRequest();
				xmlhttp_worklog.open("GET","http://192.168.164.129/rest/jira-worklog-query/1/find/worklogs?startDate=2012-12-12&group=jira-software-users&project=" + $("#project_list").val(),false);
		        xmlhttp_worklog.setRequestHeader("Content-type","application/json");
		        xmlhttp_worklog.send(null);
				var worklog = "";
		        var myWork = JSON.parse(xmlhttp_worklog.responseText);					
				try{
					$("#jira_worklog").val(JSON.stringify(myWork));
		        }
		        catch(e){
		            $("#jira_worklog").val("");
		        }	        
            });
 
		    $("#but_submit").click(function(){
		        xmlhttp.open("PUT","http://192.168.164.129/rest/api/2/issue/" + $("#the_issues").val() + "",false);
		        xmlhttp.setRequestHeader("Content-type","application/json");
			var trans_component;
			var component_data="";
			trans_component=$("#jira_component").val().split(",");
			for (var t=0;t < trans_component.length;t++) {
			    if (t>0){
				component_data = component_data + ",";
			    }
			    component_data = component_data + "{\"name\":"+trans_component[t]+"}";
			}
		        var data = "{\"update\": {\"components\":[{\"set\":["+ component_data +"]}]},\"fields\": {   \
		                \"summary\":\"" + $("#jira_summary").val() + "\",   \
		                \"assignee\":{\"name\": \"" + $("#sel_name").val() + "\"},   \
		                \"description\":\"" + $("#jira_description").val() + "\" \
		            }   \
		        }";		            
		        xmlhttp.send(data);
				xmlhttp_worklog.send(data);
		        alert(xmlhttp.responseText);
		        alert($("#the_issues").val()+"modify finish");
		    });
		    
		    $("#submit_create").click(function(){
		        xmlhttp=new XMLHttpRequest();
                xmlhttp.open("POST","http://192.168.164.129/rest/api/2/issue/",false);
                xmlhttp.setRequestHeader("Content-type","application/json");
                var createdata = "{   \
		            \"fields\": {   \
		                \"project\":{\"key\": \"" + $("#project_list").val() + "\"}, \
		                \"summary\": \"" + $("#new_issue").val() + "\",  \
		                \"description\":\"" + $("#new_description").val() + "\",   \
		                \"issuetype\":{\"name\": \"" + $("#sel_issuetype").val() + "\"} \
		            }   \
		        }";
                xmlhttp.send(createdata);
                var newissue = xmlhttp.responseText;
                alert(newissue);
                alert(JSON.stringfy(newissue.name)+"has created!");
            });
	});
	 function sleep(ms) {
		var dt = new Date();
		dt.setTime(dt.getTime() + ms);
		while (new Date().getTime() < dt.getTime())
			;
	}; 
</script>

</head>
<body onload=load()>
	<div class="container">
		<h1 class="text-center">Hello! JIRA World!</h1>
		<span class="label label-default">RestAPI Demo:2018_0619</span>
		<h2>
		  <button id="loadproject" type="button" class="btn btn-default"> Load Projects </button>
		</h2>
		<div class="row">
			<h4 class="col-md-5">
					<select id="project_list" class="form-control"><!-- <select multiple id="project_list" class="form-control"> -->
						<option value=""> please select project </option>
					</select>
			</h4>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">Project總Worklog</div>
					<div class="panel-body">
						<form role="form">
							<div class="form-group col-md-12">
								<label for="jira_worklog">Worklog</label> <textarea id="jira_worklog" class="form-control" cols="40" rows="5" placeholder="顯示 Worklog"></textarea>
							</div>
							<div class="form-group col-md-12">
							 <button type="submit" id="but_submit" class="btn btn-default"> Update </button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
            <div class="col-md-12">
                <span class="label label-primary">WebHook Demo:</span>
                <span class="label label-info">
                <a href="http://requestb.in/1ffhf131?inspect" target="_blank">http://requestb.in/1ffhf131?inspect</a>
                </span>
            </div>
        </div>
        <div class="footer">
            <div class="col-md-12 text-center">
                Copyright © 2018 Vic, Linktech Inc. All rights reserved
            </div>
        </div>
	</div>
</body>
</html>
